use std::{
    convert::TryInto,
    fmt,
    num::{NonZeroU8, NonZeroUsize},
    str::FromStr,
};

use ordinal::Ordinal;
use rand::{rngs::ThreadRng, thread_rng, Rng};

use crate::error::Error;

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
enum Season {
    Summer,
    Autumn,
    Winter,
    Spring,
}

#[derive(PartialEq, Eq, Debug, Clone)]
struct Month {
    name: &'static str,
    day: NonZeroU8,
    season: Season,
}

#[derive(PartialEq, Eq, Debug, Clone)]
pub struct Date {
    month_and_day: Month,
    year: usize,
}

impl Default for Date {
    fn default() -> Self {
        Self {
            month_and_day: Month {
                name: MONTH_ARRAY[0].name,
                day: MONTH_ARRAY[0].day,
                season: MONTH_ARRAY[0].season,
            },
            year: 0,
        }
    }
}

const fn unwrap(opt: Option<NonZeroU8>) -> NonZeroU8 {
    match opt {
        Some(x) => x,
        None => {
            panic!("Invalid NonZero")
        }
    }
}

const MONTH_ARRAY_LEN: usize = 12;
const MONTH_ARRAY: [Month; MONTH_ARRAY_LEN] = [
    Month {
        name: "Abadius",
        day: unwrap(NonZeroU8::new(31)),
        season: Season::Winter,
    },
    Month {
        name: "Calistril",
        day: unwrap(NonZeroU8::new(28)),
        season: Season::Winter,
    },
    Month {
        name: "Pharast",
        day: unwrap(NonZeroU8::new(31)),
        season: Season::Spring,
    },
    Month {
        name: "Gozran",
        day: unwrap(NonZeroU8::new(30)),
        season: Season::Spring,
    },
    Month {
        name: "Desnus",
        day: unwrap(NonZeroU8::new(31)),
        season: Season::Spring,
    },
    Month {
        name: "Sarenith",
        day: unwrap(NonZeroU8::new(30)),
        season: Season::Summer,
    },
    Month {
        name: "Erastus",
        day: unwrap(NonZeroU8::new(31)),
        season: Season::Summer,
    },
    Month {
        name: "Arodus",
        day: unwrap(NonZeroU8::new(31)),
        season: Season::Summer,
    },
    Month {
        name: "Rova",
        day: unwrap(NonZeroU8::new(30)),
        season: Season::Autumn,
    },
    Month {
        name: "Lamashan",
        day: unwrap(NonZeroU8::new(31)),
        season: Season::Autumn,
    },
    Month {
        name: "Neth",
        day: unwrap(NonZeroU8::new(30)),
        season: Season::Autumn,
    },
    Month {
        name: "Kuthona",
        day: unwrap(NonZeroU8::new(31)),
        season: Season::Winter,
    },
];

const YEAR_LENGTH: NonZeroUsize = {
    const fn unwrap(opt: Option<NonZeroUsize>) -> NonZeroUsize {
        match opt {
            Some(x) => x,
            None => {
                panic!("Invalid NonZero")
            }
        }
    }

    let mut accum: usize = 0;
    let mut index = 0;
    while index < MONTH_ARRAY_LEN {
        accum += MONTH_ARRAY[index].day.get() as usize;
        index += 1;
    }
    unwrap(NonZeroUsize::new(accum))
};

impl FromStr for Date {
    type Err = Error;

    // DDth of MMMM, YY
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let [day, _, month, year, ..] = s.split_ascii_whitespace().collect::<Vec<_>>()[..] else {
            return Err(Error::DateParse(
                "Date format must be 'DDth of MMMM, YY'".to_owned(),
            ));
        };

        let day: NonZeroU8 = day
            .chars()
            .take_while(|c| c.is_ascii_digit())
            .collect::<String>()
            .parse()
            .map_err(|e| Error::DateParse(format!("Failed to parse day: {}", e)))?;

        let month_and_day = {
            let mut month: Month = month.parse()?;
            if day > month.day {
                return Err(Error::DateParse(
                    "There aren't that many days in the month".to_owned(),
                ));
            }
            month.day = day;
            month
        };

        let year = year
            .chars()
            .take_while(|c| c.is_ascii_digit())
            .collect::<String>()
            .parse()
            .map_err(|e| Error::DateParse(format!("Failed to parse day: {}", e)))?;

        Ok(Self {
            month_and_day,
            year,
        })
    }
}

impl FromStr for Month {
    type Err = Error;

    // DDth of MMMM, YY
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        for month in MONTH_ARRAY {
            if s.to_lowercase().starts_with(&month.name.to_lowercase()) {
                return Ok(month);
            }
        }

        Err(Error::DateParse("Invalid month".to_owned()))
    }
}

impl fmt::Display for Date {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let year_name = "IA";
        write!(f, "{}, {} {}", self.month_and_day, self.year, year_name)
    }
}

impl fmt::Display for Month {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} of {}", Ordinal(self.day.get()), self.name)
    }
}

impl Month {
    fn new(day: usize) -> Self {
        // Add 1 so that:
        // - Day 0 is 1st of Abadius, year 0
        // - Day YEAR_LENGTH is 1st of Abadius, year 1
        // - Day YEAR_LENGTH - 1 is 31st of Kuthona, year 0
        let mut day = day % YEAR_LENGTH + 1;

        for month in &MONTH_ARRAY {
            if day <= month.day.get() as usize {
                // Safe to unwrap to u8 as it's <= than `month.day` with is a u8
                let day: u8 = day.try_into().unwrap();

                return Month {
                    name: month.name,
                    season: month.season,
                    // Safe to unwrap as non-zero as `day = day % YEAR_LENGTH + 1`, so is at least 1
                    //  and we only `-= month.day` when `> month.day`
                    day: NonZeroU8::new(day).unwrap(),
                };
            } else {
                day -= month.day.get() as usize;
            }
        }

        unreachable!();
    }
}

impl Date {
    pub fn new(day: usize) -> Self {
        let year = day / YEAR_LENGTH;
        let month_and_day = Month::new(day);

        Self {
            year,
            month_and_day,
        }
    }

    pub fn as_count(&self) -> usize {
        let mut day: usize = self.year * YEAR_LENGTH.get() + self.month_and_day.day.get() as usize;

        for month in &MONTH_ARRAY {
            if month.name == self.month_and_day.name {
                break;
            }

            day += month.day.get() as usize;
        }

        // Subtract 1 so that:
        // - Day 0 is 1st of Abadius, year 0
        // - Day YEAR_LENGTH is 1st of Abadius, year 1
        // - Day YEAR_LENGTH - 1 is 31st of Kuthona, year 0
        //
        // As we add at least one NoneZeroU8, it is not possible for this to wrap.
        day -= 1;

        day
    }

    /// Generates weather for the current season.
    /// https://www.d20pfsrd.com/gamemastering/environment/weather/
    /// Assumes temperate, lowland
    pub fn random_weather(&self) -> String {
        let mut rng = thread_rng();

        let (temperature, precipitation_percentage): (i8, u8) = match self.month_and_day.season {
            Season::Summer => (80, 60),
            Season::Autumn => (60, 30),
            Season::Winter => (30, 15),
            Season::Spring => (60, 30),
        };

        fn d10(rng: &mut ThreadRng) -> i8 {
            rng.gen_range(1..=10)
        }

        fn d100(rng: &mut ThreadRng) -> u8 {
            rng.gen_range(1..=100)
        }

        let temperature = temperature
            + match d100(&mut rng) {
                1..=5 => 0 - d10(&mut rng) - d10(&mut rng) - d10(&mut rng),
                6..=15 => 0 - d10(&mut rng) - d10(&mut rng),
                16..=35 => 0 - d10(&mut rng),
                36..=65 => 0,
                66..=85 => d10(&mut rng),
                86..=95 => d10(&mut rng) + d10(&mut rng),
                96..=100 => d10(&mut rng) + d10(&mut rng) + d10(&mut rng),
                val => panic!("d100 is not between 1 and 100? It was {}", val),
            };

        let is_raining = d100(&mut rng) < precipitation_percentage;

        let (overhead_weather, is_windy) = if is_raining {
            match d100(&mut rng) {
                1..=20 => ("medium fog", false),
                21..=30 => ("heavy fog", false),
                31..=100 if temperature < 32 => ("medium snow", true),
                91..=100 if temperature < 40 => ("some sleet", true),
                31..=100 => ("some rain", true),
                val => panic!("d100 is not between 1 and 100? It was {}", val),
            }
        } else {
            (
                match d100(&mut rng) {
                    1..=50 => "clear skies",
                    51..=70 => "light cloud cover",
                    71..=85 => "medium cloud cover",
                    86..=100 => "heavily overcast skies",
                    val => panic!("d100 is not between 1 and 100? It was {}", val),
                },
                true,
            )
        };

        let wind_strength = match d100(&mut rng) {
            _ if !is_windy => "no wind",
            1..=50 => "light winds",
            51..=80 => "moderate winds",
            81..=90 => "strong winds",
            91..=95 => "severe winds",
            96..=100 if d100(&mut rng) == 100 => "winds strong enough to tear windows from their frames, watch out for low flying glass,",
            96..=100 => "windstorms",
            val => panic!("d100 is not between 1 and 100? It was {}", val),
        };

        let temperature = match temperature {
            // Minimum temperature is 30 (winter) -3d10 (max of 30) = 0.
            0..=32 => "freezing cold",
            33..=40 => "chilly",
            41..=50 => "cool",
            51..=75 => "pleasantly warm",
            76..=85 => "hot",
            // Maximum temperature is 80 (summer) +3d10 (max of 30) = 110
            86..=110 => "sweltering",
            val => panic!("Unexpected temperature: {}", val),
        };

        format!(
            "The day is {}, with {} and {}",
            temperature, wind_strength, overhead_weather
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn day_0_is_first() {
        let date = Date {
            month_and_day: Month {
                name: MONTH_ARRAY[0].name,
                day: NonZeroU8::new(1).unwrap(),
                season: MONTH_ARRAY[0].season,
            },
            year: 0,
        };
        assert_eq!(Date::new(0), date);
        assert_eq!(date.as_count(), 0);
    }
    #[test]
    fn day_1_is_second() {
        let date = Date {
            month_and_day: Month {
                name: MONTH_ARRAY[0].name,
                day: NonZeroU8::new(2).unwrap(),
                season: MONTH_ARRAY[0].season,
            },
            year: 0,
        };
        assert_eq!(Date::new(1), date);
        assert_eq!(date.as_count(), 1);
    }
    #[test]
    fn last_day_of_first_month() {
        let date = Date {
            month_and_day: Month {
                name: MONTH_ARRAY[0].name,
                day: MONTH_ARRAY[0].day,
                season: MONTH_ARRAY[0].season,
            },
            year: 0,
        };
        assert_eq!(Date::new(MONTH_ARRAY[0].day.get() as usize - 1), date);
        assert_eq!(date.as_count(), MONTH_ARRAY[0].day.get() as usize - 1);
    }
    #[test]
    fn first_day_of_second_month() {
        let date = Date {
            month_and_day: Month {
                name: MONTH_ARRAY[1].name,
                day: NonZeroU8::new(1).unwrap(),
                season: MONTH_ARRAY[1].season,
            },
            year: 0,
        };
        assert_eq!(Date::new(MONTH_ARRAY[0].day.get() as usize), date);
        assert_eq!(date.as_count(), MONTH_ARRAY[0].day.get() as usize);
    }
    #[test]
    fn first_day_of_first_year() {
        let date = Date {
            month_and_day: Month {
                name: MONTH_ARRAY[0].name,
                day: NonZeroU8::new(1).unwrap(),
                season: MONTH_ARRAY[0].season,
            },
            year: 1,
        };
        assert_eq!(Date::new(YEAR_LENGTH.get()), date);
        assert_eq!(date.as_count(), YEAR_LENGTH.get());
    }
    #[test]
    fn last_day_of_0th_year() {
        let date = Date {
            month_and_day: Month {
                name: MONTH_ARRAY[MONTH_ARRAY_LEN - 1].name,
                day: MONTH_ARRAY[MONTH_ARRAY_LEN - 1].day,
                season: MONTH_ARRAY[MONTH_ARRAY_LEN - 1].season,
            },
            year: 0,
        };
        assert_eq!(Date::new(YEAR_LENGTH.get() - 1), date);
        assert_eq!(date.as_count(), YEAR_LENGTH.get() - 1);
    }

    macro_rules! parse_test {
        ($func:ident, $input:expr, $expected:pat) => {
            #[test]
            fn $func() {
                let parsed = $input.parse::<Date>();
                assert!(matches!(parsed.unwrap_err(), $expected));
            }
        };
        ($func:ident, $input:expr, $year:expr, $month:expr, $day:expr) => {
            #[test]
            fn $func() {
                let parsed = $input.parse::<Date>();
                assert_eq!(
                    parsed.unwrap(),
                    Date {
                        year: $year,
                        month_and_day: Month {
                            name: MONTH_ARRAY[$month].name,
                            day: NonZeroU8::new($day).unwrap(),
                            season: MONTH_ARRAY[$month].season,
                        },
                    }
                );
            }
        };
    }

    parse_test!(empty_string_to_date, "", Error::DateParse(_));
    parse_test!(parse_day_0, "1st of Abadius, 0 IA", 0, 0, 1);
    parse_test!(parse_day_something, "11th of Pharast, 56 IA", 56, 2, 11);
}
