use std::fmt;

use super::commands::Command;

#[derive(Debug)]
pub enum Error {
    /// Unexpected argument given to command
    UnexpectedArg(Command),
    /// Missing a required argument
    MissingArg(Command),
    /// Invalid value for argument
    InvalidArg(Command),
    /// What it says on the tin
    UnknownCommand,
    /// Requested data not found
    NoData,
    /// Error from tokio spawm
    Thread(tokio::task::JoinError),
    /// Error from I/O
    Io(std::io::Error),
    /// Error from JSON parsing
    Json(serde_json::Error),
    /// Error parsing date
    DateParse(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::UnexpectedArg(command) => {
                write!(f, "Unexpected argument for command {}", command)
            }
            Self::MissingArg(command) => write!(f, "Missing argument for command {}", command),
            Self::InvalidArg(command) => {
                write!(f, "Invalid value for argument of command {}", command)
            }
            Self::UnknownCommand => write!(f, "Unknown command"),
            Self::NoData => write!(f, "Not found"),
            Self::Thread(error) => write!(f, "Error joining threads: {}", error),
            Self::Io(error) => write!(f, "I/O error: {}", error),
            Self::Json(error) => write!(f, "JSON error: {}", error),
            Self::DateParse(error) => write!(f, "Error when parsing date: {}", error),
        }
    }
}

impl From<tokio::task::JoinError> for Error {
    fn from(err: tokio::task::JoinError) -> Self {
        Self::Thread(err)
    }
}

impl From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Self {
        Self::Io(err)
    }
}

impl From<serde_json::Error> for Error {
    fn from(err: serde_json::Error) -> Self {
        Self::Json(err)
    }
}
