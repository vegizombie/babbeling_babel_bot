use std::env;

use crate::commands::Frequency;

pub mod calendar;
pub mod commands;
pub mod error;
pub mod storage;

use async_std::task::sleep;
use calendar::Date;
use chrono::{offset::Utc, NaiveTime};
use commands::Command;
use error::Error;
use futures::stream::StreamExt;
use log::{debug, error, info, warn, LevelFilter};
use serenity::{
    async_trait,
    http::client::Http,
    model::{
        channel::Message,
        id::{ChannelId, GuildId, RoleId},
    },
    prelude::*,
};
use simple_logger::SimpleLogger;
use storage::ServerData;
use tokio::try_join;

const AUTOMATED_TICK_COUNT: usize = 2;

#[derive(Debug)]
struct Handler {}

#[async_trait]
impl EventHandler for Handler {
    // Set a handler for the `message` event - so that whenever a new message
    // is received - the closure (or function) passed will be called.
    //
    // Event handlers are dispatched through a threadpool, and so multiple
    // events can be dispatched simultaneously.
    async fn message(&self, ctx: Context, msg: Message) {
        match self.handle_message(ctx, msg).await {
            _ => (),
        }
    }
}

impl Handler {
    fn new() -> Self {
        Self {}
    }

    async fn handle_message(&self, ctx: Context, msg: Message) {
        // Ignore messages not for us.
        let prefix = "fw!";
        if let Some(cmd) = msg.content.strip_prefix(prefix) {
            if self.is_valid_user(&ctx.http, &msg).await {
                match cmd.parse() {
                    Err(e) => {
                        warn!("Failed to parse command '{}': {}", msg.content, e);
                        respond(&ctx.http, msg, &e.to_string()).await;
                    }
                    Ok(cmd) if cmd.guild_only() && msg.guild_id.is_none() => {
                        respond(&ctx.http, msg, &format!("{} must be done on a server", cmd)).await;
                    }
                    Ok(Command::Ping) => respond(&ctx.http, msg, "pong!").await,
                    Ok(Command::Channel { id }) => {
                        self.set_channel(msg.guild_id.unwrap(), id).await;
                        respond(&ctx.http, msg, &format!("Setting channel to {id}")).await
                    }
                    Ok(Command::Convert { count }) => {
                        respond(
                            &ctx.http,
                            msg,
                            &format!("{count} as a date would be {}", Date::new(count)),
                        )
                        .await
                    }
                    Ok(Command::Frequency { frequency }) => {
                        self.set_frequency(msg.guild_id.unwrap(), frequency).await;
                        respond(
                            &ctx.http,
                            msg,
                            &format!("Setting frequency of ticks to {frequency:?}",),
                        )
                        .await
                    }
                    Ok(Command::Days { count }) => {
                        if self.set_days(msg.guild_id.unwrap(), count).await {
                            respond(
                                &ctx.http,
                                msg,
                                &format!("Setting date to {}", Date::new(count)),
                            )
                            .await
                        } else {
                            respond(
                                &ctx.http,
                                msg,
                                "Failed to set date, is the channel set for the server?",
                            )
                            .await
                        }
                    }
                    Ok(Command::Tick { count }) => {
                        if fetch_and_tick(&ctx.http, msg.guild_id.unwrap(), count, true).await {
                            debug!("Manually ticked");
                            respond(&ctx.http, msg, "Manually ticked").await
                        } else {
                            respond(
                                &ctx.http,
                                msg,
                                "Failed to  tick, is the channel set for the server?",
                            )
                            .await
                        }
                    }
                    Ok(Command::Role { id }) => {
                        if self.set_role(msg.guild_id.unwrap(), id).await {
                            respond(&ctx.http, msg, &format!("Setting role to {}", id)).await
                        } else {
                            respond(
                                &ctx.http,
                                msg,
                                "Failed to set role, is the channel set for the server?",
                            )
                            .await
                        }
                    }
                    Ok(Command::Pause) => {
                        if self.set_paused(msg.guild_id.unwrap(), true).await {
                            respond(&ctx.http, msg, "Paused, please resume later with `resume`")
                                .await
                        } else {
                            respond(
                                &ctx.http,
                                msg,
                                "Failed to pause, is the channel set for the server?",
                            )
                            .await
                        }
                    }
                    Ok(Command::Resume) => {
                        if self.set_paused(msg.guild_id.unwrap(), false).await {
                            respond(&ctx.http, msg, "Resumed").await
                        } else {
                            respond(
                                &ctx.http,
                                msg,
                                "Failed to resume, is the channel set for the server?",
                            )
                            .await
                        }
                    }
                    Ok(Command::Date { date }) => {
                        if self.set_days(msg.guild_id.unwrap(), date.as_count()).await {
                            respond(&ctx.http, msg, &format!("Setting date to {}", date)).await
                        } else {
                            respond(
                                &ctx.http,
                                msg,
                                "Failed to set date, is the channel set for the server?",
                            )
                            .await
                        }
                    }
                    Ok(Command::TickLength { length }) => {
                        if self.set_tick_length(msg.guild_id.unwrap(), length).await {
                            respond(&ctx.http, msg, &format!("Setting tick length to {length}"))
                                .await
                        } else {
                            respond(
                                &ctx.http,
                                msg,
                                "Failed to pause, is the channel set for the server?",
                            )
                            .await
                        }
                    }
                }
            }
        }
    }

    async fn set_frequency(&self, guild_id: GuildId, frequency: Frequency) -> bool {
        if let Ok(data) = ServerData::retrieve(guild_id).await {
            data.set_frequency(frequency)
                .store()
                .await
                .expect("Couldn't store server data...");
            true
        } else {
            false
        }
    }

    async fn is_valid_user(&self, http: &Http, msg: &Message) -> bool {
        if let Some(guild_id) = msg.guild_id {
            let maybe_data = ServerData::retrieve(guild_id).await;
            if let Ok(data) = maybe_data {
                if let Some(role) = data.allowed_role() {
                    return msg
                        .author
                        .has_role(http, guild_id, role)
                        .await
                        .expect("Under what conditions does this fail?");
                }
            }
        }

        true
    }

    async fn set_channel(&self, guild_id: GuildId, channel: ChannelId) {
        let data = if let Ok(data) = ServerData::retrieve(guild_id).await {
            data.set_channel(channel)
        } else {
            ServerData::new(guild_id, channel)
        };

        data.store().await.expect("Couldn't store server data...");
    }

    async fn set_role(&self, guild_id: GuildId, role: RoleId) -> bool {
        if let Ok(data) = ServerData::retrieve(guild_id).await {
            data.set_role(role)
                .store()
                .await
                .expect("Couldn't store server data...");
            true
        } else {
            false
        }
    }

    async fn set_days(&self, guild_id: GuildId, days: usize) -> bool {
        if let Ok(data) = ServerData::retrieve(guild_id).await {
            data.set_date(days)
                .store()
                .await
                .expect("Couldn't store server data...");
            true
        } else {
            false
        }
    }

    async fn set_tick_length(&self, guild_id: GuildId, length: usize) -> bool {
        if let Ok(data) = ServerData::retrieve(guild_id).await {
            data.set_tick_length(length)
                .store()
                .await
                .expect("Couldn't store server data...");
            true
        } else {
            false
        }
    }

    async fn set_paused(&self, guild_id: GuildId, paused: bool) -> bool {
        if let Ok(data) = ServerData::retrieve(guild_id).await {
            data.set_paused(paused)
                .store()
                .await
                .expect("Couldn't store server data...");
            true
        } else {
            false
        }
    }
}

async fn fetch_and_tick(client: &Http, guild_id: GuildId, count: usize, manual_tick: bool) -> bool {
    let data = ServerData::retrieve(guild_id).await;
    if let Ok(data) = data {
        let res = tick(client, data, count, manual_tick).await;
        if let Ok(v) = res {
            v
        } else {
            warn!("Failed to tick {}", res.unwrap_err());
            false
        }
    } else {
        warn!("Failed to retrieve server data {}", data.unwrap_err());
        false
    }
}

async fn tick(
    client: &Http,
    data: ServerData,
    count: usize,
    manual_tick: bool,
) -> Result<bool, serenity::Error> {
    if !data.is_paused() || manual_tick {
        let data = data.tick(count);
        let date = data.get_date();
        let _msg_result = data
            .get_channel()
            .say(
                client,
                format!(
                    "@here Today is the {} ({} days)\n\n{}",
                    date,
                    count,
                    date.random_weather()
                ),
            )
            .await?;

        data.store().await.expect("Couldn't store server data...");
        return Ok(true);
    }

    Ok(false)
}

struct Sleeper {
    client: Http,
}

impl Sleeper {
    fn new(token: &str) -> Self {
        Self {
            client: Http::new_with_token(token),
        }
    }

    async fn spin(self) -> Result<(), Error> {
        loop {
            let now = Utc::now();
            let today = now.date_naive();
            let time = NaiveTime::from_hms_opt(14, 00, 0).expect("14:00:00 is a valid time");
            let next_tick = match today.and_time(time).and_utc() {
                next if now < next => next,
                _ => today
                    .succ_opt()
                    .expect("Tomorrow exists")
                    .and_time(time)
                    .and_utc(),
            };
            debug!("Next tick at {}", next_tick);
            let pause = next_tick - now;
            debug!("Countdown to next tick: {}", pause);
            sleep(
                pause
                    .to_std()
                    .expect("Somehow a negative period of time away, despite match"),
            )
            .await;
            debug!("Sleep finished at: {}", Utc::now());

            let mut stream = ServerData::stream().await.expect("Failed to fetch data");
            while let Some(data) = stream.next().await.transpose()? {
                if data.should_tick_at(today) {
                    let _ = tick(&self.client, data, AUTOMATED_TICK_COUNT, false).await;
                }
            }
        }
    }
}

async fn respond(client: &Http, msg: Message, text: &str) {
    debug!("Responding with {}", text);
    if let Err(why) = msg.channel_id.say(client, text).await {
        warn!("Error sending message: {:?}", why)
    }
}

async fn start_client(mut client: Client) -> Result<(), Error> {
    tokio::spawn(async move {
        if let Err(e) = client.start().await {
            error!("Client error: {:?}", e);
        }
    })
    .await
    .map_err(Error::Thread)
}

#[tokio::main]
async fn main() {
    let token = env::var("DISCORD_TOKEN").expect("Expected a token in the environment");

    // Print logs of level Debug and above for our code, and Info and above in dependencies
    SimpleLogger::new()
        .with_level(LevelFilter::Warn)
        .with_module_level("babel", LevelFilter::Debug)
        .init()
        .expect("Failed to set up logging!");

    let handler = Handler::new();
    let sleeper = Sleeper::new(&token);

    // Create a new instance of the Client, logging in as a bot. This will
    // automatically prepend your bot token with "Bot ", which is a requirement
    // by Discord for bot users.
    let client = Client::builder(&token)
        .event_handler(handler)
        .await
        .expect("Err creating client");

    // Finally, start a single shard, and start listening to events.
    //
    // Shards will automatically attempt to reconnect, and will perform
    // exponential backoff until it reconnects.
    info!("Starting up");
    // Put the client in its own thread, otherwise it doesn't release the await and the sleeper
    // never gets to spin.
    try_join!(start_client(client), sleeper.spin()).expect("Failed");
}
